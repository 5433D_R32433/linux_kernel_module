#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX_PATH 4096

int get_fullpath_of_process(const char *process_name) {
    pid_t pid = getpid();
    char proc_path[32];
    char exe_path[MAX_PATH];

    // Construct the path to the /proc directory for the current process
    snprintf(proc_path, sizeof(proc_path), "/proc/%d/exe", pid);

    // Use readlink to get the full path of the executable
    ssize_t len = readlink(proc_path, exe_path, sizeof(exe_path) - 1);

    if (len != -1) {
        exe_path[len] = '\0';  // Null-terminate the string
        printf("Full path of the process %s: %s\n", process_name, exe_path);
        return 0;
    } else {
        perror("Error reading executable path");
        return -1;
    }
}

int main() {
    if (get_fullpath_of_process("ls") == 0) {
        // Continue with your program logic
    } else {
        // Handle the error
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

