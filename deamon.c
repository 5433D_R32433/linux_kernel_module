#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>

#define DEVICE_PATH "/dev/ftrace_hook"
#define OUTPUT_FILE "/tmp/ftrace_hook.log"

void daemonize ( ) 
{
    pid_t pid, sid;

    pid = fork ( );
    if ( pid < 0 ) 
    {
        exit ( EXIT_FAILURE );
    }
    
    umask ( 0 );

    sid = setsid ( );
    if ( sid < 0 ) 
    {
        exit ( EXIT_FAILURE );
    }

    if ( ( chdir ( "/" ) ) < 0 ) 
    {
        exit ( EXIT_FAILURE );
    }

    close ( STDIN_FILENO  );
    close ( STDOUT_FILENO );
    close ( STDERR_FILENO );
}

void write_to_file ( const char *data ) 
{
    int fd = open ( OUTPUT_FILE, O_WRONLY | O_CREAT | O_APPEND, 0644 );
    if ( fd == -1 ) 
    {
        perror ( "error opening file" );
        exit   ( EXIT_FAILURE         );
    }
    write ( fd, data, strlen ( data ) );
    close ( fd );
}

void signal_handler ( int signal_number ) 
{
    switch ( signal_number ) 
    {
        case SIGTERM:
        case SIGINT:
	{
	    exit ( EXIT_SUCCESS );
	} break;
    }
}

int main ( ) 
{
    daemonize();

    signal ( SIGTERM, signal_handler );
    signal ( SIGINT,  signal_handler );

    while ( 1 ) 
    {
        int fd = open ( DEVICE_PATH, O_RDONLY );
        if ( fd == -1 ) 
	{
            perror ( "Error opening device" );
            exit   ( EXIT_FAILURE );
        }
        char buffer [ 4096 ] = { 0 };
        ssize_t bytesRead = read ( fd, buffer, sizeof ( buffer ) );
        if ( bytesRead > 0 ) 
	{
            write_to_file ( buffer );
        }
        close ( fd );
        sleep ( 1 );
    }

    return 0;
}

