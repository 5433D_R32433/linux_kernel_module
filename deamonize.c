#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>

#define BD_NO_CHDIR          01 
#define BD_NO_CLOSE_FILES    02 
#define BD_NO_REOPEN_STD_FDS 04 
#define BD_NO_UMASK0        010 
#define BD_MAX_CLOSE       8192 

// returns 0 on success -1 on error
int daemonize ( int flags )
{
  int maxfd, fd;

  switch ( fork ( ) )
  {
        case -1: 
    	{
		return -1;
	} break;

    	case 0: 
	{

	} break;
    	
	default: 
	{
		_exit ( EXIT_SUCCESS );
	}
  }

  if ( setsid ( ) == -1 )
  {
	  return -1;
  }
  
  switch ( fork ( ) )
  {
	  case -1: 
	  { 
		    return -1;
	  } break;

    	  case 0: 
	  {

	  } break;

    	  default: 
	  {
		  _exit ( EXIT_SUCCESS );
	  } break;	  
  }

  if ( !( flags & BD_NO_UMASK0 ) )
  {
	  umask ( 0 );
  }	  

  if ( !( flags & BD_NO_CHDIR ) )
  {
	  chdir ( "/" );
  }	  

  if( !( flags & BD_NO_CLOSE_FILES ) ) 
  {
    maxfd = sysconf ( _SC_OPEN_MAX );
    if ( maxfd == -1 )
    {
	    maxfd = BD_MAX_CLOSE;
    }

    for ( fd = 0; fd < maxfd; fd++ )
    {
	    close ( fd );
    }
  }

  if ( !( flags & BD_NO_REOPEN_STD_FDS ) )
  {
    close ( STDIN_FILENO );
    
    fd = open ( "/dev/null", O_RDWR );
    if ( fd != STDIN_FILENO )
    {
	    return -1;
    }

    if ( dup2 ( STDIN_FILENO, STDOUT_FILENO ) != STDOUT_FILENO )
    {
	    return -2;
    }

    if ( dup2 ( STDIN_FILENO, STDERR_FILENO ) != STDERR_FILENO )
    {
	    return -3;
    }
  }

  return 0;
}

#define LOG_FILE "/tmp/ftrace_hook.log"
#define DEVICE_PATH "/dev/ftrace_hook"


void write_log ( const char *data )
{
    int fd = open ( LOG_FILE, O_WRONLY | O_CREAT | O_APPEND, 0644 );
    if ( fd == -1 )
    {
        perror ( "error opening file" );
        exit   ( EXIT_FAILURE         );
    }
    write ( fd, data, strlen ( data ) );
    close ( fd );
}


int main ( int argc, char **argv )
{
  int ret;

  if ( getuid ( ) != 0 )
  {
	  fprintf ( stderr, "You must boot root to run this deamon!\n" );
	  exit    ( EXIT_FAILURE );
  }

  ret = daemonize ( 0 );
  if ( ret )
  {
    return EXIT_FAILURE;
  }

  while ( 1 )
  {
    sleep  ( 5 );
    int fd = open ( DEVICE_PATH, O_RDONLY );
    if ( fd != -1 )
    {
	   char buffer [ 4096 ] = { 0 };
    	   ssize_t bytesRead = read ( fd, buffer, sizeof ( buffer ) );
           if ( bytesRead > 0 )
           {
		   write_log ( buffer );
    	   }

  	   close ( fd );
    }
  }
  return EXIT_SUCCESS;
}
