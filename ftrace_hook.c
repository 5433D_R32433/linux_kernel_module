#define pr_fmt(fmt) "ftrace_hook: " fmt
#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/kprobes.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/proc_fs.h>
#include <linux/limits.h>
#include <linux/binfmts.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/fdtable.h>
#include <linux/fs_struct.h>
#include <linux/path.h>
#include <linux/dcache.h>

MODULE_DESCRIPTION ( "Example module hooking via ftrace" );
MODULE_AUTHOR      ( "Saeed Rezaee <saeed@rezaee.net>"   );
MODULE_LICENSE     ( "GPL v2" );

#define BUFFER_SIZE ( 64 * 1024 )
static char circular_buffer [ BUFFER_SIZE ] = { 0 };
static int  buffer_write_offset             = 0;

#define IOCTL_SET_PATHS _IOWR('x', 1, path_info_t)
#define IOCTL_SET_PROGRAMS _IOWR('x', 2, path_info_t)

#define MAX_PATH_LENGTH	    256
#define MAX_NUMBER_OF_PATHS 16
typedef struct path_info_t 
{
    int num_paths;
    char paths [ MAX_NUMBER_OF_PATHS ] [ MAX_PATH_LENGTH ];
} path_info_t;

path_info_t user_programs = { 0 };
path_info_t user_paths    = { 0 };

void write_log ( const char *fmt, ... ) 
{
    va_list args;
    int written;
    va_start(args, fmt);
    written = vsnprintf ( circular_buffer + buffer_write_offset, BUFFER_SIZE - buffer_write_offset, fmt, args );
    va_end(args);
    buffer_write_offset = ( buffer_write_offset + written ) % BUFFER_SIZE;
}

#if 0
asmlinkage long write_to_userspace_file ( struct file *file, const char *data, size_t length )
{
	if ( file )
    	{
		ssize_t written = kernel_write ( file, data, length, 0 );
    		if ( written < 0 ) 
    		{
        		pr_err ( "Failed to write to file\n" );
        		// filp_close ( file, NULL );
        		return written;
    		}
		return 0;
	}
	else
	{
		pr_err ( "log file is not opened!\n" );
		return -ENOMEM;
	}
}
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,7,0)
static unsigned long lookup_name ( const char *name )
{
	struct kprobe kp = 
	{
		.symbol_name = name
	};

	unsigned long ret;

	if ( register_kprobe (&kp ) < 0 ) 
	{
		return 0;
	}

	ret = ( unsigned long ) kp.addr;
	unregister_kprobe ( &kp );
	return ret;
}
#else
static unsigned long lookup_name ( const char *name )
{
	return kallsyms_lookup_name ( name );
}
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,11,0)
#define FTRACE_OPS_FL_RECURSION FTRACE_OPS_FL_RECURSION_SAFE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,11,0)
#define ftrace_regs pt_regs

static __always_inline struct pt_regs *ftrace_get_regs ( struct ftrace_regs *fregs )
{
	return fregs;
}
#endif

#define USE_FENTRY_OFFSET 0

struct ftrace_hook 
{
	const char    *name;
	void          *function;
	void          *original;
	unsigned long address;
	struct        ftrace_ops ops;
};

static int ftrace_resolve_hook_address ( struct ftrace_hook *hook )
{
	hook->address = lookup_name ( hook->name );
	if ( !hook->address ) 
	{
		pr_debug ( "unresolved symbol: %s\n", hook->name );
		return -ENOENT;
	}
#if USE_FENTRY_OFFSET
	*( ( unsigned long* ) hook->original ) = hook->address + MCOUNT_INSN_SIZE;
#else
	*( ( unsigned long* ) hook->original ) = hook->address;
#endif
	return 0;
}

static void notrace ftrace_thunk ( unsigned long ip, 
		                   unsigned long parent_ip, 
			           struct ftrace_ops *ops, 
			           struct ftrace_regs *fregs )
{
	struct pt_regs *regs = ftrace_get_regs  ( fregs );
	struct ftrace_hook *hook = container_of ( ops, struct ftrace_hook, ops );
#if USE_FENTRY_OFFSET
	regs->ip = ( unsigned long ) hook->function;
#else
	if ( !within_module ( parent_ip, THIS_MODULE ) )
	{
		regs->ip = ( unsigned long ) hook->function;
	}
#endif
}

int ftrace_install_hook ( struct ftrace_hook *hook )
{
	int err;
	err = ftrace_resolve_hook_address ( hook );
	if ( err )
	{
		return err;
	}
	
	hook->ops.func = ftrace_thunk;
	hook->ops.flags = FTRACE_OPS_FL_SAVE_REGS
	                | FTRACE_OPS_FL_RECURSION
	                | FTRACE_OPS_FL_IPMODIFY;

	err = ftrace_set_filter_ip ( &hook->ops, hook->address, 0, 0 );
	if ( err ) 
	{
		pr_debug ( "ftrace_set_filter_ip() failed: %d\n", err );
		return err;
	}

	err = register_ftrace_function ( &hook->ops );
	if ( err ) 
	{
		pr_debug ( "register_ftrace_function ( ) failed: %d\n", err );
		ftrace_set_filter_ip ( &hook->ops, hook->address, 1, 0 );
		return err;
	}

	return 0;
}

void ftrace_remove_hook ( struct ftrace_hook *hook )
{
	int err;

	err = unregister_ftrace_function ( &hook->ops );
	if ( err ) 
	{
		pr_debug ( "unregister_ftrace_function ( ) failed: %d\n", err );
	}

	err = ftrace_set_filter_ip ( &hook->ops, hook->address, 1, 0 );
	if ( err ) 
	{
		pr_debug ( "ftrace_set_filter_ip ( ) failed: %d\n", err );
	}
}

int ftrace_install_hooks ( struct ftrace_hook *hooks, size_t count )
{
	int err;
	size_t i;
	for (i = 0; i < count; i++) 
	{
		err = ftrace_install_hook ( &hooks [ i ] );
		if ( err )
		{
			goto error;
		}
	}
	return 0;

error:
	while (i != 0 ) 
	{
		ftrace_remove_hook ( &hooks [ --i ] );
	}
	return err;
}

void ftrace_remove_hooks ( struct ftrace_hook *hooks, size_t count )
{
	size_t i;

	for ( i = 0; i < count; i++ )
	{
		ftrace_remove_hook ( &hooks [ i ] );
	}
}

#ifndef CONFIG_X86_64
#error Currently only x86_64 architecture is supported
#endif

#if defined(CONFIG_X86_64) && (LINUX_VERSION_CODE >= KERNEL_VERSION(4,17,0))
#define PTREGS_SYSCALL_STUBS 1
#endif

#if !USE_FENTRY_OFFSET
#pragma GCC optimize("-fno-optimize-sibling-calls")
#endif

#if 0

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long ( *real_sys_clone ) ( struct pt_regs *regs );

static asmlinkage long ftrace_hook_sys_clone ( struct pt_regs *regs )
{
	long ret;
	ret = real_sys_clone(regs );
	pr_info ( "clone(): %ld\n", ret );
	return ret;
}
#else
static asmlinkage long ( *real_sys_clone )   ( unsigned long clone_flags, 
		                               unsigned long newsp, 
					       int __user *parent_tidptr,
					       int __user *child_tidptr, 
					       unsigned long tls );

static asmlinkage long ftrace_hook_sys_clone ( unsigned long clone_flags,
					       unsigned long newsp, 
					       int __user *parent_tidptr,
					       int __user *child_tidptr, 
					       unsigned long tls )
{
	long ret;

	ret = real_sys_clone ( clone_flags, newsp, parent_tidptr, child_tidptr, tls );
	pr_info ( "clone(): %ld\n", ret );

	return ret;
}
#endif
#endif

static char *duplicate_filename ( const char __user *filename )
{
	char *kernel_filename;

	kernel_filename = kmalloc ( 4096, GFP_KERNEL );
	if ( !kernel_filename )
	{
		return NULL;
	}
	if ( strncpy_from_user ( kernel_filename, filename, 4096 ) < 0 ) 
	{
		kfree ( kernel_filename );
		return NULL;
	}

	return kernel_filename;
}


const char *get_process_state ( unsigned int state )
{
	switch ( state )
	{
		case TASK_RUNNING:
		{
			return "RUNNING";
		} break;
		case TASK_INTERRUPTIBLE:
		{
			return "INTERRUPTIBLE";
		} break;

		case TASK_UNINTERRUPTIBLE:
		{
			return "UNINTERRUPTIBLE";
		} break;

		case __TASK_STOPPED:
		{
			return "STOPPED";
		} break;

		case __TASK_TRACED:
		{
			return "TRACED";
		} break;	
		
		case TASK_PARKED:
		{
			return "PACKED";
		} break;	
		case TASK_DEAD:
		{
			return "DEAD";
		} break;	

		case TASK_WAKEKILL:
		{
			return "WAKEKILL";
		} break;	

		case TASK_WAKING:
		{
			return "WAKING";
		} break;

		case TASK_NOLOAD:
		{
			return "NOLOAD";
		} break;	
		case TASK_NEW:
		{
			return "NEW";
		} break;

		case TASK_RTLOCK_WAIT:
		{
			return "RTLOCK_WAIT";
		} break;	

		case TASK_FREEZABLE:
		{
			return "FREEZABLE";
		} break;

		case TASK_FROZEN:
		{
			return "FROZEN";
		} break;

		default:
		{
			return "UNKNOWN";
		} break;	
	}
}


static void ls_procs ( void )
{
    struct task_struct *task       = 0;
    struct task_struct *task_child = 0; 
    struct list_head *list         = 0; 

    for_each_process ( task )
    {
	pr_info ( "\nPARENT PID: %d PROCESS: %s STATE: %s", task->pid, task->comm, get_process_state ( task->__state ) );
        list_for_each ( list, &task->children )
	{
            task_child = list_entry ( list, struct task_struct, sibling );
            pr_info ( "\nCHILD OF %s[%d] PID: %d PROCESS: %s STATE: %s",task->comm, task->pid, task_child->pid, task_child->comm, get_process_state ( task_child->__state ) );
        }
        printk("-----------------------------------------------------"); 
    }
}

static void print_open_files ( struct task_struct *task ) 
{
	struct files_struct *current_files = 0;
 	struct fdtable *files_table        = 0;
	int i                              = 0;
 	struct path files_path;
 	char *cwd                          = 0;
 	char *buffer                       = ( char* ) kmalloc ( GFP_KERNEL, 100 * sizeof ( char ) );
 	current_files                      = task->files;
        files_table                        = files_fdtable ( current_files );
 	while ( files_table->fd [ i ] != NULL ) 
 	{
 		files_path = files_table->fd [ i ]->f_path;
 		cwd = d_path ( &files_path, buffer, 100 * sizeof ( char ) );
 		pr_info ( "Open file with fd %d  %s", i, cwd );
 		++i;
	}
	kfree ( buffer );
}


static void print_process_tree ( struct task_struct *task, int level ) 
{
    struct task_struct *child_task;
    struct list_head *list;
    
    pr_info ( "%*sPID: %d, Name: %s\n", level * 2, "", task->pid, task->comm );
    
    list_for_each ( list, &task->children ) 
    {
        child_task = list_entry ( list, struct task_struct, sibling );
        print_process_tree      ( child_task, level + 1 );
    }
}


typedef struct argument_t
{
	char **argv;
	int    argc;
} argument_t;


typedef struct environment_variable_t
{
	char **envp;
	int    envc;
} environment_variable_t;


argument_t get_arguments ( void** argv )
{
	argument_t args = { 0 };
	int i           = 0;
    	for ( ;; ) 
	{ 
		size_t arg_ptr = ( size_t ) argv + i * sizeof ( size_t );
        	size_t user_arg;
		if ( copy_from_user ( &user_arg, ( void* ) arg_ptr, sizeof ( size_t ) ) ) 
		{
            		continue;
        	}
        	if ( user_arg == 0 ) 
		{
			break;
        	} 
		else 
		{
			args.argc = i + 1;	
			if ( !args.argv )
			{
				args.argv = ( char** ) kmalloc ( args.argc * sizeof ( char* ), 
						                 GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN );
			}
			else
			{
				args.argv = ( char** ) krealloc ( ( char** ) args.argv, 
				 		                  args.argc * sizeof ( char* ), 
								  GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN );
			}

			*( args.argv + i ) = ( char* ) kmalloc ( PATH_MAX * sizeof ( char ), 
					                      GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN | __GFP_ZERO );
            		
			if ( copy_from_user ( *( args.argv + i ), ( void* ) user_arg, PATH_MAX ) ) 
			{
            		} 
			else 
			{
                		// args.argv [ i ] [ MAX_ARG_STRLEN - 1 ] = '\0';
				*( *( args.argv + i ) + PATH_MAX - 1 )  = '\0';
            		}
        	}
		++i;
    	}
	return args;
}

void unget_arguments ( argument_t *args )
{
	for ( int i = 0; i < args->argc; i++ )
	{
		kfree ( *( args->argv + i ) );
		*( args->argv + i ) = 0;
	}
	kfree ( args->argv );
	args->argv = 0;
}


environment_variable_t get_environment_variables ( void** envp )
{
	environment_variable_t ev = { 0 };
	int i                     = 0;
    	for ( ;; ) 
	{ 
		size_t env_ptr = ( size_t ) envp + i * sizeof ( size_t );
        	size_t user_env;
		if ( copy_from_user ( &user_env, ( void* ) env_ptr, sizeof ( size_t ) ) ) 
		{
            		continue;
        	}
        	if ( user_env == 0 ) 
		{
			break;
        	} 
		else 
		{
			ev.envc = i + 1;	
			if ( !ev.envp )
			{
				ev.envp = ( char** ) kmalloc ( ev.envc * sizeof ( char* ), 
						               GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN );
			}
			else
			{
				ev.envp = ( char** ) krealloc ( ( char** ) ev.envp, 
						                ev.envc * sizeof ( char* ), 
								GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN );
			}

			*( ev.envp + i ) = ( char* ) kmalloc ( PATH_MAX * sizeof ( char ), 
					                       GFP_KERNEL | GFP_ATOMIC | __GFP_NOWARN | __GFP_ZERO );

            		if ( copy_from_user ( *( ev.envp + i ), ( void* ) user_env, PATH_MAX ) != 0 ) 
			{
				pr_info ( "why didn't happen?\n" );
            		} 
			else 
			{
				*( *( ev.envp + i ) + PATH_MAX - 1 ) = '\0';
            		}
        	}
		++i;
    	}
	return ev;
}

void unget_environment_variables ( environment_variable_t *ev )
{
	for ( int i = 0; i < ev->envc; i++ )
	{
		kfree ( ev->envp [ i ] );
	}
	kfree ( ev->envp );
}

#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long ( *real_sys_execve )   ( struct pt_regs *regs );
static asmlinkage long ftrace_hook_sys_execve ( struct pt_regs *regs )
{
	long ret;
	char *kernel_filename;
	kernel_filename = duplicate_filename ( ( void* ) regs->di );

	// ls_procs ( );

	pr_info ( "*** %s ***\n", kernel_filename );
	argument_t args = get_arguments ( ( void** ) regs->si );
	
	for ( int i = 0; i < args.argc; i++ )
	{
		pr_info ( "%d --> %s\n", i, args.argv [ i ] );
	}
	
	
	unget_arguments ( &args ); 


	environment_variable_t ev = get_environment_variables ( ( void** ) regs->dx );

	for ( int i = 0; i < ev.envc; i++ )
	{
		pr_info ( "%d -->>> %s\n", i, ev.envp [ i ] );
	}

	unget_environment_variables ( &ev );




#if 0
	char *envs = ( char* ) kmalloc ( MAX_ARG_STRLEN, GFP_KERNEL );
	if ( !envs )
	{
		pr_err ( "kmalloc failed!\n" );
		return -ENOMEM;
	}
	memset ( envs, 0, MAX_ARG_STRLEN );
	
	i = 0;
	for ( ;; ) 
	{ 
        	unsigned long env_ptr = regs->dx + i * sizeof ( unsigned long );
        	unsigned long user_env;

		if ( copy_from_user ( &user_env, ( void* ) env_ptr, sizeof ( unsigned long ) ) ) 
		{
            		pr_info ( "Failed to copy argument %d from user space\n", i + 1 );
            		continue;
        	}

        	if ( user_env == 0 ) 
		{
            		// pr_info ( "Environment Variable %d: NULL\n", i + 1 );
			break;
        	} 
		else 
		{
            		if ( copy_from_user ( envs, ( void* ) user_env, MAX_ARG_STRLEN ) ) 
			{
                		pr_info ( "Failed to copy string argument %d from user space\n", i + 1 );
            		} 
			else 
			{
                		envs [ MAX_ARG_STRLEN - 1 ] = '\0';
                		pr_info ( "Environment Variable %d: %s\n", i + 1, envs );
            		}
        	}
		++i;
    	}


	struct path pwd;
    	char *cwd    = NULL;
    	char *buffer = NULL;

    	get_fs_pwd ( current->fs, &pwd );
   	buffer = kmalloc ( PATH_MAX, GFP_ATOMIC | __GFP_NOWARN | __GFP_ZERO );
    	
	if ( buffer ) 
	{
        	cwd = d_path ( &pwd, buffer, PATH_MAX );
        	// printk ( KERN_ALERT "The current working directory: %s\n", cwd );
    	}

	for ( int i = 0; i < user_paths.num_paths; i++ )
	{
		if ( !strncmp ( cwd, user_paths.paths [ i ], strlen ( user_paths.paths [ i ] ) ) )
		{
			pr_info ( "[WARNING]: process %s making", "fuck" );
		}
	}
#endif


	// struct task_struct *task = current;
	pr_info ( "Process %s ( pid: %d ) is calling execve.\n", current->comm, current->pid );
	// print_process_tree ( task, 0 );
	// print_open_files ( task );

#if 0
	struct task_struct *task;
    	for_each_process ( task ) 
	{
            	pr_info ( "PID: %d, Name: %s\n", task->pid, task->comm );
#if 0
		if ( strcmp ( task->comm, "ls" ) == 0 ) 
		{
        	}
#endif
	}
	pr_info ( "-----------------------------------------\n" );
#endif

        for ( int i = 0; i < user_programs.num_paths; i++ )
        {
		if ( !strncmp ( kernel_filename, user_programs.paths [ i ], strlen ( user_programs.paths [ i ]  ) ) )
		{
			pr_info ( "[WARNING] Program %s is used!\n", user_programs.paths [ i ] );
		}
        }

	// write_log ( "execve ( %s )\n", kernel_filename );
	
	kfree   ( kernel_filename );
	
	// kfree   ( args    );
	// kfree   ( envs    );
        // kfree   ( buffer  );
	//
        ret = real_sys_execve ( regs );
	return ret;
}
#else
static asmlinkage long ( *real_sys_execve )   ( const char __user *filename,
					      	const char __user *const __user *argv,
					      	const char __user *const __user *envp );

static asmlinkage long ftrace_hook_sys_execve ( const char __user *filename,
						const char __user *const __user *argv,
						const char __user *const __user *envp)
{
	long ret;
	char *kernel_filename;
	kernel_filename = duplicate_filename ( filename );
	
	write_log ( "execve ( %s )\n", kernel_filename );
	
	kfree ( kernel_filename );
	ret = real_sys_execve ( filename, argv, envp );
	
	return ret;
}
#endif


#ifdef PTREGS_SYSCALL_STUBS
static asmlinkage long ( *real_sys_openat   ) ( struct pt_regs *regs );
static asmlinkage long ftrace_hook_sys_openat ( struct pt_regs *regs )
{
	long ret;
	for ( int i = 0; i < user_paths.num_paths; i++ )	
	{
		char *kernel_filename;
		kernel_filename = duplicate_filename ( ( void* ) regs->si );
		
		if ( !strncmp ( kernel_filename, user_paths.paths [ i ], strlen ( user_paths.paths [ i ] ) ) )
		{	
			pr_info ( "[WARNING] openat ( %s )\n", user_paths.paths [ i ] );
			kfree ( kernel_filename );
		}
	}

	ret = real_sys_openat ( regs );
	return ret;
}
#else

static asmlinkage long ( *real_sys_openat )  (  int dirfd, 
		                                const char __user *pathname,
					        int flags,
					        mode_t mode );

static asmlinkage long ftrace_hook_sys_openat ( int dirfd,
						const char __user *pathname,
		                                int flags,
		                                mode_t mode )
{
	long ret;
	for ( int i = 0; i < user_paths.num_paths; i++ )	
	{
		char *kernel_filename;
		kernel_filename = duplicate_filename ( ( void* ) pathname );
		
		if ( !strncmp ( kernel_filename, ,user_paths.paths [ i ], strlen ( user_paths.paths [ i ] ) ) )
		{	
			pr_info ( "[WARNING] openat ( %s )\n", user_paths.paths [ i ] );
			kfree ( kernel_filename );
		}
	}

	ret = real_sys_openat ( dirfd, pathname, flags, mode );
	return ret;
}
#endif

#ifdef PTREGS_SYSCALL_STUBS
#define SYSCALL_NAME(name) ("__x64_" name)
#else
#define SYSCALL_NAME(name) (name)
#endif

#define HOOK( _name, _function, _original )	\
	{					\
		.name = SYSCALL_NAME( _name ),	\
		.function = ( _function ),	\
		.original = ( _original ),	\
	}

static struct ftrace_hook hooks [ ] = 
{
	// HOOK ( "sys_clone",  ftrace_hook_sys_clone,  &real_sys_clone  ),
	HOOK ( "sys_execve", ftrace_hook_sys_execve, &real_sys_execve ),
	HOOK ( "sys_openat", ftrace_hook_sys_openat, &real_sys_openat ),
};


#if 0
static ssize_t proc_file_write ( struct file *file, const char __user *buffer, size_t count, loff_t *offset )
{
    return count;
}

static ssize_t proc_file_read ( struct file* file, char __user *buffer, size_t count, loff_t *offset )
{
    char *text = "Hello World From Kernel!\n";
    int to_copy, not_copied, delta_copy;
    to_copy = min ( count, strlen ( text ) );
    not_copied = copy_to_user ( buffer, text, to_copy );
    delta_copy = to_copy - not_copied;
    return delta_copy;
}

static const struct proc_ops pops = 
{
    .proc_write = proc_file_write,
    .proc_read  = proc_file_read,
};
#endif

long ftrace_hook_ioctl ( struct file *file, unsigned int cmd, unsigned long arg )
{
    switch ( cmd )
    {
        case IOCTL_SET_PATHS:
        {
            if ( copy_from_user ( &user_paths, ( path_info_t* ) arg, sizeof ( path_info_t ) ) )
            {
                return -EFAULT;
            }

            pr_info ( "<<RECEIVED PATHS INFO>>\n" );
            pr_info ( "Number of Paths: %d\n", user_paths.num_paths );

            for ( int i = 0; i < user_paths.num_paths; ++i )
            {
                pr_info ( "Path %03d. %s\n", i, user_paths.paths [ i ] );
            }

        } break;

        case IOCTL_SET_PROGRAMS:
        {
            if ( copy_from_user ( &user_programs, ( path_info_t* ) arg, sizeof ( path_info_t ) ) )
            {
                return -EFAULT;
            }

            pr_info ( "<<RECEIVED PROGRAMS INFO>>\n" );
            pr_info ( "Number of Programs: %d\n", user_programs.num_paths );

            for ( int i = 0; i < user_programs.num_paths; ++i )
            {
                pr_info(  "Program Path %03d. %s\n", i, user_programs.paths [ i ] );
            }
        } break;
    }

    return 0;
}

static int ftrace_hook_open ( struct inode *inode, struct file *file ) 
{
    pr_info ( "ftrace_hook device opened!\n" );
    return 0;
}

static int ftrace_hook_release ( struct inode *inode, struct file *file ) 
{
    pr_info ( "ftrace_hook device closed!\n" );
    return 0;
}


static ssize_t ftrace_hook_read ( struct file *file, char *buffer, size_t length, loff_t *offset ) 
{
    size_t bytes_to_copy   = 0;
    size_t remaining_bytes = 0;

    remaining_bytes = buffer_write_offset - *offset;

    if ( remaining_bytes <= 0 )
    {
	    return 0;
    }
    
    bytes_to_copy = min ( length, remaining_bytes );

    if ( copy_to_user ( buffer, circular_buffer + *offset, bytes_to_copy ) ) 
    {
	pr_err ( "copy_to_user() failed!\n" );
        return -EFAULT;
    }
    *offset += bytes_to_copy;
    return bytes_to_copy;
}

static ssize_t ftrace_hook_write ( struct file *file, const char *buffer, size_t length, loff_t *offset ) 
{
    pr_info ( "%s", "Just for fun and profit!\n" );
    return strlen ( "Just for fun and profit!\n" );
}

static const struct file_operations ftrace_hook_fops = 
{
    .open           = ftrace_hook_open,
    .release        = ftrace_hook_release,
    .read           = ftrace_hook_read,
    .write          = ftrace_hook_write,
    .unlocked_ioctl = ftrace_hook_ioctl,
};

static int    major_number; 
static struct class*  ftrace_hook_class  = NULL; 
static struct device* ftrace_hook_device = NULL;

static int ftrace_hook_init ( void )
{
#if 0
	proc_folder = proc_mkdir ( "dummy", NULL );
	if ( !proc_folder )
	{
		pr_err ( "proc_mkdir failed!\n" );
		return -ENOMEM;
	}

	proc_file = proc_create ( "foo", 0666, proc_folder, &pops );
	if ( !proc_file )
	{
		pr_err ( "proc_create failed!\n" );
		proc_remove ( proc_folder );
		return -ENOMEM;
	}

    	log_file = filp_open ( "/tmp/ftrace_kernel_module.log", O_WRONLY | O_CREAT | O_TRUNC, 0644 );
    	if ( IS_ERR ( log_file ) ) 
	{
        	pr_err ( "Failed to open file\n" );
        	return PTR_ERR ( log_file );
    	}
#endif

	int err;
	err = ftrace_install_hooks ( hooks, ARRAY_SIZE ( hooks ) );
	if ( err )
	{
		return err;
	}
	pr_info ( "module loaded\n" );


   	major_number = register_chrdev ( 0, "ftrace_hook", &ftrace_hook_fops );
   	if ( major_number < 0 )
	{
      		pr_alert ( "ftrace_hook failed to register a major number\n");
      		return major_number;
   	}
   	pr_info ( "ftrace_hook: registered correctly with major number %d\n", major_number );


   	ftrace_hook_class = class_create ( "fh" );
   	if ( IS_ERR ( ftrace_hook_class ) ) 
	{           
      		unregister_chrdev ( major_number, "ftrace_hook" );
      		pr_alert ( "Failed to register device class\n"  );
      		return PTR_ERR ( ftrace_hook_class );
   	}
   	pr_info ( "ftrace_hook: device class registered correctly\n" );

   	ftrace_hook_device = device_create ( ftrace_hook_class, NULL, MKDEV ( major_number, 0 ), NULL, "ftrace_hook" );
   	if ( IS_ERR ( ftrace_hook_device ) )
   	{
      		class_destroy ( ftrace_hook_class ); 
      		unregister_chrdev ( major_number, "ftrace_hook" );
      		pr_alert ( "Failed to create the device\n" );
      		return PTR_ERR ( ftrace_hook_device );
   	}
   
	pr_info ( "ftrace_hook: device class created correctly\n" );
   	return 0;
}

static void ftrace_hook_exit ( void )
{
	ftrace_remove_hooks ( hooks, ARRAY_SIZE ( hooks ) );
#if 0
	pr_info ( "removing /proc/dummy/foo\n" );

	if ( proc_file )
	{
		proc_remove ( proc_file );
	}
	if ( proc_folder )
	{
		proc_remove ( proc_folder );
	}

	if ( log_file )
	{
    		filp_close ( log_file, NULL );
	}
#endif
   	device_destroy    ( ftrace_hook_class, MKDEV ( major_number, 0 ) );  
   	class_unregister  ( ftrace_hook_class );                         
   	class_destroy     ( ftrace_hook_class );                          
   	unregister_chrdev ( major_number, "ftrace_hook" );
	pr_info           ( "ftrace_hook module unloaded\n" );
}

module_init ( ftrace_hook_init );
module_exit ( ftrace_hook_exit );

