#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <asm/segment.h>
#include <linux/kstrtox.h>
#include <linux/kallsyms.h>
#include <linux/kprobes.h>
#include <asm/set_memory.h>


static struct kprobe kp = 
{
    .symbol_name = "kallsyms_lookup_name"
};

MODULE_LICENSE     ( "GPL"                             );
MODULE_AUTHOR      ( "Saeed Rezaee <saeed@rezaee.net>" );
MODULE_DESCRIPTION ( "A Kernel Module"                 );

#define MAX_LEN_ENTRY 1024
#define PROC_KALLSYMS "/proc/kallsyms"

static unsigned long __force_order;

unsigned long *get_syscall_table_by_proc ( void )
{
	char *filename                           = PROC_KALLSYMS;
	int i                                    = 0;
	struct file *proc_ksyms                  = NULL;
	char *sct_addr_str                       = NULL;
	char proc_ksyms_entry [ MAX_LEN_ENTRY ]  = { 0 };
	unsigned long* res                       = NULL;
	char *proc_ksyms_entry_ptr               = NULL;
	int read                                 = 0;

#if LINUX_VERSION_CODE <= KERNEL_VERSION(5,0,0)
	mm_segment_t oldfs;
#endif
	


	if ( ( sct_addr_str = ( char* ) kmalloc ( MAX_LEN_ENTRY * sizeof ( char ), GFP_KERNEL ) ) == NULL )
	{
		goto CLEAN_UP;
	}

	if ( ( ( proc_ksyms = filp_open ( filename, O_RDONLY, 0 ) ) ) == NULL )
	{
		goto CLEAN_UP;
	}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,0,0)
	read = kernel_read ( proc_ksyms, proc_ksyms_entry + i, 1, &( proc_ksyms->f_pos ) );
#else
	ENTER_KERNEL_ADDR_SPACE ( oldfs );
	read = vfs_read ( proc_ksyms, proc_ksyms_entry + i, 1, &( proc_ksyms->f_pos ) );
	EXIT_KERNEL_ADDR_SPACE  ( oldfs );
#endif

	while ( read == 1)
	{
		if ( proc_ksyms_entry [ i ] == '\n' || i == MAX_LEN_ENTRY )
		{
			if ( strstr ( proc_ksyms_entry, "sys_call_table" ) != NULL )
			{
				printk ( KERN_INFO "Found Syscall table\n" );
				printk ( KERN_INFO "Line is:%s\n", proc_ksyms_entry );

				proc_ksyms_entry_ptr = proc_ksyms_entry;
				strncpy ( sct_addr_str, strsep(&proc_ksyms_entry_ptr, " " ), MAX_LEN_ENTRY );
				if ( ( res = kmalloc ( sizeof ( unsigned long ), GFP_KERNEL ) ) == NULL )
				{
					goto CLEAN_UP;
				}
				if ( kstrtoul ( sct_addr_str, 16, res ) != 0 )
				{
					printk ( KERN_ERR "kstrtoul failed!\n" );
					goto CLEAN_UP;
				}
				printk ( "Syscall Table Address: %lu\n", *res );
				goto CLEAN_UP;
			}

			i = -1;
			memset ( proc_ksyms_entry, 0, MAX_LEN_ENTRY );
		}

		i++;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,0,0)
	read = kernel_read ( proc_ksyms, proc_ksyms_entry + i, 1, &( proc_ksyms->f_pos ) );
#else
	ENTER_KERNEL_ADDR_SPACE ( oldfs );
	read = vfs_read ( proc_ksyms, proc_ksyms_entry + i, 1, &( proc_ksyms->f_pos ) );
	EXIT_KERNEL_ADDR_SPACE  ( oldfs );
#endif
	}

CLEAN_UP:
	if ( sct_addr_str != NULL )
	{
		kfree ( sct_addr_str );
	}
	if ( proc_ksyms != NULL )
	{
		filp_close ( proc_ksyms, 0 );
	}

	return ( unsigned long* ) res;
}



asmlinkage long ( *original_sys_open ) ( const char __user *filename, int flags, umode_t mode );

asmlinkage long hooked_sys_open ( const char __user *filename, int flags, umode_t mode )
{
    char buffer [ 256 ] = { 0 };
    if ( copy_from_user ( buffer, filename, sizeof ( buffer ) ) != 0 ) 
    {
        return -EFAULT;
    }

    printk ( KERN_INFO "File %s Opened!\n", buffer );

    return original_sys_open ( filename, flags, mode );
}

inline void hacked_write_cr0 ( unsigned long cr0 ) 
{
    asm volatile ( "mov %0,%%cr0" : "+r" ( cr0 ), "+m" ( __force_order ) );
}



static int __init kernel_module_init ( void )
{
    printk ( KERN_INFO "Kernel Module Loaded Successfully!\n" );

    typedef unsigned long ( *kallsyms_lookup_name_t ) ( const char *name );
    kallsyms_lookup_name_t kallsyms_lookup_name;
    register_kprobe  ( &kp );
    kallsyms_lookup_name = ( kallsyms_lookup_name_t ) kp.addr;
    unregister_kprobe ( &kp );

    unsigned long address = kallsyms_lookup_name ( "sys_call_table" );
    printk ( KERN_INFO "sycall table address: 0x%lX\n", address );

    void **syscall_table = ( void* ) address;
    original_sys_open = syscall_table [ __NR_open ];
    
    hacked_write_cr0 ( read_cr0 ( ) & ( ~0x10000 ) );
    
    syscall_table [ __NR_open ] = ( unsigned long* ) hooked_sys_open;

    hacked_write_cr0 ( read_cr0 ( ) | 0x10000 );


    return 0;
}



static void __exit kernel_module_exit ( void )
{

    typedef unsigned long ( *kallsyms_lookup_name_t ) ( const char *name );
    kallsyms_lookup_name_t kallsyms_lookup_name;
    register_kprobe  ( &kp );
    kallsyms_lookup_name = ( kallsyms_lookup_name_t ) kp.addr;
    unregister_kprobe ( &kp );


    unsigned long address = kallsyms_lookup_name ( "sys_call_table" );

    void **syscall_table = ( void* ) address;
    
    hacked_write_cr0 ( read_cr0 ( ) & ( ~0x10000 ) );
    
    if ( syscall_table )
    {
        syscall_table [ __NR_open ] = ( unsigned long* ) original_sys_open;
    }
    hacked_write_cr0(read_cr0() | 0x10000);

    printk ( KERN_INFO "Kernel Module Unloaded Successfully!\n" );
}

module_init ( kernel_module_init );
module_exit ( kernel_module_exit );
