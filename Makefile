obj-m += ftrace_hook.o

obj-m += ftrace_hook.o
MY_CFLAGS += -g
ccflags-y += ${MY_CFLAGS}
CC += ${MY_CFLAGS}

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

debug:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

EXTRA_CFLAGS="$(MY_CFLAGS)"
clean: 
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

