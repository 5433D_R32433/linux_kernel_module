#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

#define IOCTL_SET_PATHS    _IOWR('x', 1, struct path_info_t)
#define IOCTL_SET_PROGRAMS _IOWR('x', 2, struct path_info_t)

#define MAX_PATH_LENGTH     256
#define MAX_NUMBER_OF_PATHS 16
struct path_info_t 
{
    int num_paths;
    char paths [ MAX_NUMBER_OF_PATHS ] [ MAX_PATH_LENGTH ];
};

int main ( int argc, char **argv ) 
{
    if ( getuid ( ) != 0 )
    
    {
	    fprintf ( stderr, "You Must be root!\n" );
	    exit    ( EXIT_FAILURE );
    }

    if ( argc < 5 ) 
    {
        fprintf ( stderr, "Usage: %s --program <number_of_programs> <program1> <program2> ... --paths <number_of_paths> <path1> <path2> ...\n", argv [ 0 ] );
        exit    ( EXIT_FAILURE );
    }

    int fd = open ( "/dev/ftrace_hook", O_RDWR );
    if ( fd == -1 ) 
    {
        perror ( "Failed to open the device!\n" );
        exit   ( EXIT_FAILURE );
    }

    struct path_info_t programs_info = { 0 };

    int num_programs = atoi ( argv [ 2 ] );
    programs_info.num_paths = num_programs > MAX_NUMBER_OF_PATHS ? MAX_NUMBER_OF_PATHS : num_programs;

    for ( int i = 0; i < programs_info.num_paths; ++i ) 
    {
        strncpy ( programs_info.paths [ i ], argv[i + 3 ], MAX_PATH_LENGTH - 1 );
        programs_info.paths [ i ] [ MAX_PATH_LENGTH - 1 ] = '\0';
    }

    if ( ioctl ( fd, IOCTL_SET_PROGRAMS, &programs_info ) == -1 ) 
    {
        perror ( "IOCTL for programs failed" );
        close  ( fd );
        exit   ( EXIT_FAILURE );
    }

    struct path_info_t paths_info = { 0 };
    int num_paths = atoi ( argv [ 4 + num_programs ] );
    
    paths_info.num_paths = num_paths > MAX_NUMBER_OF_PATHS ? MAX_NUMBER_OF_PATHS : num_paths;

    for ( int i = 0; i < paths_info.num_paths; ++i ) 
    {
        strncpy ( paths_info.paths [ i ], argv [ i + 5 + num_programs ], MAX_PATH_LENGTH - 1 );
        paths_info.paths [ i ] [ MAX_PATH_LENGTH - 1 ] = '\0';
    }

    if ( ioctl ( fd, IOCTL_SET_PATHS, &paths_info ) == -1 ) 
    {
        perror ( "IOCTL for paths failed" );
        close  ( fd );
        exit   ( EXIT_FAILURE );
    }

    printf     ( "IOCTL requests sent successfully.\n" );
    close ( fd );
    return 0;
}

