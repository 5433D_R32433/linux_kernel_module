#include <stdio.h>

int main ( int argc, char **argv, char **envp )
{
	while ( *envp )
	{
		fprintf ( stdout, "%s\n", *envp );
		++envp;
	}
	return 0;
}
