#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <dirent.h>

#define IOCTL_SET_PATH _IOW('x', 1, char *)
#define IOCTL_SET_PROG _IOW('x', 2, char *)

char **get_path ( )
{
    const char *variable_name = "PATH";
    char *path                = getenv ( variable_name );
    char* input_copy          = strdup ( path );
    int num_parts             = 1;

    for ( int i = 0; input_copy [ i ] != '\0'; i++ )
    {
        if ( input_copy [ i ] == ':' )
        {
            num_parts++;
        }
    }
    char** parts = ( char** ) malloc ( num_parts * sizeof ( char* ) );
    char* token  = strtok ( input_copy, ":" );
    int i        = 0;

    while ( token != NULL )
    {
        parts [ i ] = strdup ( token );
        i++;
        token = strtok ( NULL, ":" );
    }

    return parts;
}



char* get_fullpath ( const char *file_name )
{
    char **path = get_path ( );
    for ( int i = 0; path [ i ] != NULL; i++ )
    {
        DIR *dir = opendir ( path [ i ] );
        if ( dir == NULL )
        {
            perror ( "opendir" );
            continue;
        }
        struct dirent *entry;
        while ( ( entry = readdir ( dir ) ) != NULL )
        {
            if ( !strcmp ( entry->d_name, file_name ) )
            {
                closedir ( dir );
                size_t fullpath_length = strlen ( path [ i ] ) + strlen ( "/" ) + strlen ( file_name ) + 1;
                char *fullpath = ( char* ) malloc ( fullpath_length );
                snprintf ( fullpath, fullpath_length, "%s/%s", path [ i ], file_name );
                return fullpath;
            }
        }
        closedir ( dir );
    }

    return NULL;
}

int main ( int argc, char **argv, char **envp )
{
	if ( getuid ( ) != 0 )
	{
		fprintf ( stderr, "You must be root to run this program!\n\n" );
		exit ( EXIT_FAILURE );
	}

	int fd = open ( "/dev/ftrace_hook", O_RDWR );
    	if ( fd == -1 ) 
	{
        	perror ( "Error opening device" );
        	return -1;
    	}

	// char *fullpath = get_fullpath ( "cat" );
	char *fullpath = "/home/samurai/saeed.txt";

    	if ( ioctl ( fd, IOCTL_SET_PATH, fullpath ) == -1 ) 
	{
        	perror ( "IOCTL failed" );
        	close ( fd );
        	return -1;
    	}

    	close ( fd );

	printf ( "brk ( NULL ): %d\n", brk ( NULL ) );
	printf ( "brk ( NULL ): %d\n", brk ( NULL ) );

	return 0;
}
