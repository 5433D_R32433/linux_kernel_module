#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

char **get_path ( )
{
    const char *variable_name = "PATH";
    char *path                = getenv ( variable_name );
    char* input_copy          = strdup ( path );

    int num_parts = 1;
    for ( int i = 0; input_copy [ i ] != '\0'; i++ ) 
    {
        if ( input_copy [ i ] == ':' ) 
	{
            num_parts++;
        }
    }

    char** parts = ( char** ) malloc ( num_parts * sizeof ( char* ) );
    char* token  = strtok ( input_copy, ":" );
    int i        = 0;

    while ( token != NULL ) 
    {
        parts [ i ] = strdup ( token );
        i++;
        token = strtok ( NULL, ":" );
    }

    return parts;
}



char* get_fullpath ( const char *file_name ) 
{
    char **path = get_path ( );
    for ( int i = 0; path [ i ] != NULL; i++ ) 
    {
        DIR *dir = opendir ( path [ i ] );
        if ( dir == NULL ) 
	{
            perror ( "opendir" );
            continue;
        }
        struct dirent *entry;
        while ( ( entry = readdir ( dir ) ) != NULL ) 
	{
            if ( !strcmp ( entry->d_name, file_name ) ) 
	    {
                closedir ( dir );
                size_t fullpath_length = strlen ( path [ i ] ) + strlen ( "/" ) + strlen ( file_name ) + 1;
                char *fullpath = ( char* ) malloc ( fullpath_length );
                snprintf ( fullpath, fullpath_length, "%s/%s", path [ i ], file_name );
                return fullpath;
            }
        }
        closedir ( dir );
    }

    return NULL;
}






int main() 
{

    printf ( "fullpath: %s\n", get_fullpath ( "cat" ) );
    printf ( "fullpath: %s\n", get_fullpath ( "sh" ) );
    printf ( "fullpath: %s\n", get_fullpath ( "echo" ) );
    printf ( "fullpath: %s\n", get_fullpath ( "vim" ) );
    return 0;
}
